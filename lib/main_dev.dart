import 'package:flutter/material.dart';

import 'app.dart';
import 'app_config.dart';

void main() async {
  AppConfig devAppConfig = AppConfig(appName: 'FlutterFlavor Dev', flavor: 'dev');
  Widget app = await initializeApp(devAppConfig);
  runApp(app);
}
