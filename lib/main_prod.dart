import 'package:flutter/material.dart';

import 'app.dart';
import 'app_config.dart';

void main() async {
  AppConfig devAppConfig = AppConfig(
      appName: 'FlutterFlavor Prod', flavor: 'prod');
  Widget app = await initializeApp(devAppConfig);
  runApp(app);
}
